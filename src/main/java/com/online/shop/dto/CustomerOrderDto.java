package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class CustomerOrderDto {


    private String customerName;
    private List<ChosenProductDto> chosenProductDtosList;
}
