package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShoppingCartIdemDto {

    @ToString.Exclude
    private String image;
    private String name;
    private String quantity;
    private  String price;
    private String total;
}
